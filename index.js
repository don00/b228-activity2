console.log('Hello World!');

//without the use of objects, our students from before would be organized as follows if we are to record additional information about them

//create student one
// let studentOneName = 'John';
// let studentOneEmail = 'john@mail.com';
// let studentOneGrades = [89, 84, 78, 88];

// //create student two
// let studentTwoName = 'Joe';
// let studentTwoEmail = 'joe@mail.com';
// let studentTwoGrades = [78, 82, 79, 85];

// //create student three
// let studentThreeName = 'Jane';
// let studentThreeEmail = 'jane@mail.com';
// let studentThreeGrades = [87, 89, 91, 93];

// //create student four
// let studentFourName = 'Jessie';
// let studentFourEmail = 'jessie@mail.com';
// let studentFourGrades = [91, 89, 92, 93];

// //actions that students may perform will be lumped together
// function login(email){
//     console.log(`${email} has logged in`);
// }

// function logout(email){
//     console.log(`${email} has logged out`);
// }

// function listGrades(grades){
//     grades.forEach(grade => {
//         console.log(grade);
//     })
// }

//This way of organizing employees is not well organized at all.
//This will become unmanageable when we add more employees or functions
//To remedy this, we will create objects


let studentOne = {
	name: "Joe",
	email: 'joe@mail.com',
	grades: [89, 84, 78, 88],

	login(){
    	console.log(`${this.email} has logged in`);
	},

	logout(){
    	console.log(`${this.email} has logged out`);
	},
		
	listGrades(){
	    this.grades.forEach(grade => {
	        console.log(grade);
	    })
	},

	computeAve(){
		const sum = this.grades.reduce((x,y) => x + y);
		return sum / this.grades.length;
	},

	willPass(){
		return this.computeAve() >= 85;
	},

	willPassWithHonors(){
		const average = this.computeAve();
		if(average >= 90) return true;
		return (average >= 85)? false: undefined;
	}
}

let studentTwo = {
	name: "John",
	email: 'john@mail.com',
	grades: [ 89, 84, 7, 88],

	login(){
    	console.log(`${this.email} has logged in`);
	},

	logout(){
    	console.log(`${this.email} has logged out`);
	},
		
	listGrades(){
	    this.grades.forEach(grade => {
	        console.log(grade);
	    })
	},

	computeAve(){
		const sum = this.grades.reduce((x,y) => x + y);
		return sum / this.grades.length;
	},

	willPass(){
		return this.computeAve() >= 85;
	},

	willPassWithHonors(){
		const average = this.computeAve();
		if(average >= 90) return true;
		return (average >= 85)? false: undefined;
	}
}

let studentThree = {
	name: "Jane",
	email: 'jane@mail.com',
	grades: [87, 89, 91, 93],

	login(){
    	console.log(`${this.email} has logged in`);
	},

	logout(){
    	console.log(`${this.email} has logged out`);
	},
		
	listGrades(){
	    this.grades.forEach(grade => {
	        console.log(grade);
	    })
	},
	computeAve(){
		const sum = this.grades.reduce((x,y) => x + y);
		return sum / this.grades.length;
	},

	willPass(){
		return this.computeAve() >= 85;
	},

	willPassWithHonors(){
		const average = this.computeAve();
		if(average >= 90) return true;
		return (average >= 85)? false: undefined;
	}
}

let studentFour = {
	name: 'Jessie',
	email: 'jessie@mail.com',
	grades: [91, 89, 92, 93],

	login(){
    	console.log(`${this.email} has logged in`);
	},

	logout(){
    	console.log(`${this.email} has logged out`);
	},
		
	listGrades(){
	    this.grades.forEach(grade => {
	        console.log(grade);
	    })
	},
	computeAve(){
		const sum = this.grades.reduce((x,y) => x + y);
		return sum / this.grades.length;
	},

	willPass(){
		return this.computeAve() >= 85;
	},

	willPassWithHonors(){
		const average = this.computeAve();
		if(average >= 90) return true;
		return (average >= 85)? false: undefined;
	}
}

// 1. Translate the other students from our boilerplate code into their own respective objects.
console.log(studentThree);
console.log(studentFour);

// 2. Define a method for EACH student object that will compute for their grade average (total of grades divided by 4)
console.log(studentOne.computeAve());
console.log(studentTwo.computeAve());
console.log(studentThree.computeAve());
console.log(studentFour.computeAve());

// 3. Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail. For a student to pass, their ave. grade must be greater than or equal to 85.
console.log(studentOne.willPass());
console.log(studentTwo.willPass());
console.log(studentThree.willPass());
console.log(studentFour.willPass());

// 4. Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than or equal to 90, false if >= 85 but < 90, and undefined if < 85 (since student will not pass).
console.log(studentOne.willPassWithHonors());
console.log(studentTwo.willPassWithHonors());
console.log(studentThree.willPassWithHonors());
console.log(studentFour.willPassWithHonors());

const classOf1A = {
	students: [studentOne , studentTwo, studentThree, studentFour],
	countHonorStudents(){
		const honorStudents = this.students.filter(student => student.willPassWithHonors());
		return honorStudents.length;
	},
	honorsPercentage(){
		return (this.countHonorStudents()/this.students.length)*100;
	},
	retrieveHonorStudentInfo(){
		const honorStudents = this.students.filter(student => student.willPassWithHonors());
		return honorStudents.map(student => {
			return {
				aveGrade: student.computeAve(),
				email: student.email
			}
		});
	},

	sortHonorStudentsByGradeDesc(){
		const honorStudentsInfo = this.retrieveHonorStudentInfo();
		return honorStudentsInfo.sort((student1, student2) => student2.aveGrade - student1.aveGrade);
	}
}
// 5. Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.
console.log(classOf1A);

// 6. Create a method for the object classOf1A named countHonorStudents() that will return the number of honor students.
console.log(classOf1A.countHonorStudents());

// 7. Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.
console.log(classOf1A.honorsPercentage());

// 8. Create a method for the object classOf1A named retrieveHonorStudentInfo() that will return all honor students' emails and ave. grades as an array of objects.
console.log(classOf1A.retrieveHonorStudentInfo());

// 9. Create a method for the object classOf1A named sortHonorStudentsByGradeDesc() that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.
console.log(classOf1A.sortHonorStudentsByGradeDesc());

/*
	Quiz
	What is the term given to unorganized code that's very hard to work with?
	spaghetti code

	How are object literals written in JS?
	const newObj = {}

	What do you call the concept of organizing information and functionality to belong to an object?
	Encapsulation
	
	If the studentOne object has a method named enroll(), how would you invoke it?
	studentOne.enroll();

	True or False: Objects can have objects as properties.
	true
	
	What is the syntax in creating key-value pairs?
	const obj = {
		key1: "value1",
		key2: "value2"
	}

	True or False: A method can have no parameters and still work.
	true

	True or False: Arrays can have objects as elements.
	true

	True or False: Arrays are objects.
	true

	True or False: Objects can have arrays as properties.
	true
*/


